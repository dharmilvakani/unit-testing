const request = require("supertest")
const jwt = require("jsonwebtoken")
const mongoose = require("mongoose")
const app = require("../app")
const User = require("../models/user")


const userOneId = new mongoose.Types.ObjectId()

const userOne = {
    _id: userOneId,
    name: "gavin ",
    email: "gavin@gmail.com",
    password: "gavin12345",
    tokens: [{
        token: jwt.sign({ _id: userOneId }, "thisismynewcourse")
    }]
}
beforeEach(async () => {
    await User.deleteMany()
    await new User(userOne).save()
})

test('should signup a new user', async () => {
    await request(app).post('/users').send({
        name: "Dharmil",
        email: "dharmil@gmail.com",
        password: "dharmil123"
    }).expect(201)

})

test("should login existing user", async () => {
    await request(app).post("/users/login").send({
        email: userOne.email,
        password: userOne.password
    }).expect(200)
})

test("should not login nonexisting user", async () => {
    await request(app).post("/users/login").send({
        email: userOne.email,
        password: "thisismypassword"
    }).expect(400)
})

test("should get profile for user", async () => {
    await request(app)
        .get("/users/me")
        .set("Authorization", `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test("Should not get profile or unauthenticated user", async () => {
    await request(app)
        .get("/users/me")
        .send()
        .expect(401)
})

test("should delete account for user", async () => {
    await request(app)
        .delete("/users/me")
        .set("Authorization", `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test("Should not delete account for unauthenticate user", async () => {
    await request(app)
        .delete("/users/me")
        .send()
        .expect(401)
})