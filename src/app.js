const express = require("express");
require("./db/mongoose");
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json')

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use(express.json())
app.use("/users", require('./routers/user'))
app.use("/tasks", require("./routers/task"))

module.exports = app
